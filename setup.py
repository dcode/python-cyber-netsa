#!/usr/bin/env python

# Copyright 2012 Derek Ditch

# @OPENSOURCE_HEADER_START@
# Use of the Cyber NetSA Python support library and related source 
# code is subject to the terms of the following licenses:
# 
# GNU Public License (GPL) Rights pursuant to Version 2, June 1991
# Government Purpose License Rights (GPLR) pursuant to DFARS 252.227.7013
# 
# @OPENSOURCE_HEADER_END@

from netsa import dist

dist.set_name("cyber-netsa")
dist.set_version("0.1")
dist.set_copyright("2012, Derek DItch")

dist.set_title("Cyber NetSA Python")
dist.set_description("""
    A set of command-line utilities and Python support libraries
    useful for the Cyber SA project. These utilities are specifically
    focused on extending the CMU NetSA tools to provide additional
    functionality for the Cyber SA project.
""")

dist.set_maintainer("Derek Ditch <derek.ditch@gmail.com>")

dist.set_url("https://bitbucket.org/dcode/python-cyber-netsa")

dist.set_license("GPL")

dist.add_package("cyber")

dist.add_version_file("src/cyber/VERSION")

dist.add_extra_files("GPL.txt")
dist.add_extra_files("CHANGES")

dist.execute()
