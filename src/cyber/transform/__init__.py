# Copyright 2012 by Derek Ditch <derek.ditch@gmail.com>

"""
The :mod:`cyber.transform` module contains transform different data sources
into a common model that can be used for data enrichment operations.  Core 
'cyber' modules and command line scripts will utilize these backend modules 
to do the heavy lifting.
"""

