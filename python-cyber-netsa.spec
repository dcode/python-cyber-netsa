# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-cyber-netsa
Version:        0.0.3
Release:        1%{?dist}
Summary:        Command line utilities for Cyber SA project using NetSA and CollectiveIntel frameworks.

Group:          Applications/System
License:        GPLv2
URL:            https://bitbucket.org/dcode/python-cyber-netsa
Source0:        https://bitbucket.org/dcode/python-cyber-netsa/downloads/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel netsa-python

Requires:       netsa-python silk-common

%description
    A set of command-line utilities and Python support libraries
    useful for the Cyber SA project. These utilities are specifically
    focused on extending the CMU NetSA tools to provide additional
    functionality for the Cyber SA project.


%prep
%setup -q


%build
# Remove CFLAGS=... for noarch packages (unneeded)
#CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
# For noarch packages: sitelib
%{python_sitelib}/*
# For arch-specific packages: sitearch
#%{python_sitearch}/*


%changelog
* Wed Dec 05 2012 Derek Ditch <derek.ditch@gmail.com> 0.0.3-1
- Updated specfile to remove release tag from tarball.

* Wed Dec 05 2012 Derek Ditch <derek.ditch@gmail.com> 0.0.2-1
- new package built with tito
- build using baseline skeleton for project

