#!/usr/bin/env python

# Import the script framework under the name "script".
from netsa import script, data
import netsa_silk

from brolog.BroLogManager import BroLogManager

from datetime import *
from exceptions import NotImplementedError

import logging

import pprint

# Set up the metadata for the script, including the title, what it
# does, who wrote it, who to ask questions about it, etc.
script.set_title("Bro Filter")
script.set_description("""
    A script that emulates the rwfilter capabilities across
    a bro log set. Additional query parameters are added in
    order to integrate the extended metadata that bro logs.
""")
script.set_version("0.1")
script.set_contact("Derek Ditch <derek.ditch@gmail.com>")
script.set_authors(["Derek Ditch <derek.ditch@gmail.com>"])

# Include start-date and end-date params
script.add_flow_params(require_pull=True, )

script.add_dir_param("bro-logdir", "Absolute path to directory containing the bro logs (i.e. bro/logs/)", required=True)
script.add_output_dir_param("output-dir", "Directory to output hourly files (binary SiLK by default).", required=False )
script.add_output_file_param("output-path", "Filename for combined output (binary SiLK flows by default)", 
        mime_type='application/x-silk-flows', required=False)

# Provide optional switch to output csv instead of binary SiLK results
script.add_flag_param("csv", "Generate output as CSV rather than binary SiLK file(s).")

# Allow parallel conversion
script.add_int_param("parallel", "Increase the number of processes to use for data pull and conversion.", expert=True, default=1, default_help="1", minimum=1)

class Mapper( object ):
    def __init__ ( self, *args ):
        raise NotImplementedError("The Mapper class provides only a common interface.")

    def __call__ (self, *args):
        raise NotImplementedError("The Mapper class provides only a common interface.")


class LogMapper( Mapper ):
    def __init__(self, brolog_root ):
        self.log_path = brolog_root

    """
    Given a set of flow params, return an hourly list of tuples of (datetime, filename).
    """
    def __call__(self, flow_params):
        result = []

        # Map files to the hourly datetime
        for _params in flow_params.by_hour():
            result.append( self.get_filelist( _params ))

        return result

    def get_filelist( self, flow_params ):
        from os import walk
        from os.path import join, abspath
        from datetime import timedelta

        filelist = []
        sdate = flow_params.get_start_date()
        edate = flow_params.get_end_date()

        sday  = sdate.date().isoformat()
        stime = sdate.time().isoformat()
        eday  = edate.date().isoformat()
        etime = edate.time().isoformat()

        logging.info("Start Date/Time: %sT%s", sday, stime)
        logging.info("End Date/Time: %sT%s", eday, etime)

        for root, dirs, files in walk( join(self.log_path, sday)) :
            # Drill down into dates in our window
            for name in files:
                if name.startswith("."): continue

                logging.debug("get_filelist(): (root, name): (%s, %s)", root, name)
                fstart = name.split('.')[1].split('-')[0]
                fend = name.split('.')[1].split('-')[1]

                if ( stime <= fstart) and ( etime >= fstart ):
                    logging.debug("Selected file: %s", join(root,name))
                    filelist.append( ( data.times.make_datetime("%sT%s" % (sday, stime)), abspath( join(root,name)) ))

        return filelist


"""
Group the sublists of (datetime, filename) pairs into a time-based 
file map, so that the Reduce operation later can work on sorted
filenames. The returned result is a dictionary with the structure
{datetime : [(datetime, filename), ...] .. }
"""
def LogPartition(L, binsize ):
    hf = {}
    for sublist in L:
        for entry in sublist:
            if len(entry) == 0:
                continue
            
            for dt, item in entry:
                try:
                    _bin = data.times.bin_datetime( binsize, dt )
                    hf[ _bin ].append(item)
                except KeyError:
                    hf[ _bin ] = [item]

    return hf

"""
Given a (datetime, (datetime, filename) ...]) tuple, collapse all the
tuples from the Mapper operation into a 
number for this token, and return a final tuple (token, frequency).
"""
class Reducer( object  ):

    def __init__(self, _outdir, _binsize):
        self.outdir = _outdir
        self.binsize = _binsize

    def __call__(self, _mapping ):
        manager = BroLogManager()

        logging.info(_mapping)
        manager.load(_mapping)

        conn = manager["conn"]
        if not conn:
            return

        sdate = datetime.fromtimestamp(conn.get_stats('ts').min)

        logging.info("Outdir: %s", self.outdir)
        logging.info("filename: %s", sdate.strftime("conn-%Y%m%d.%H.rwf"))
        self.output = script.get_output_dir_file_name( self.outdir, multiprocessing.current_process().name + ".rwf" )
        conn.export( SilkFileExporter, self.output )

        return

def mkdir_p( path ):
    import os, errno

    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

###### End Map Reduce #######

def partition(lst, n):
    q, r = divmod(len(lst), n)
    indices = [q*i + min(i, r) for i in xrange(n+1)]
    return [lst[indices[i]:indices[i+1]] for i in xrange(n)]

def main():
    from multiprocessing import Pool

    brolog_root = script.get_param("bro-logdir")
    outFile = script.get_param("output-path") 
    outDir = script.get_param("output-dir")
    numProcs = script.get_param("parallel")
    doCSV = script.get_param("csv")

    if script.get_verbosity() > 1:
        logging.basicConfig(level=logging.DEBUG)
    elif script.get_verbosity() > 0:
        logging.basicConfig(level=logging.INFO)

    if outFile and not (outFile == "stdout" or outFile == "-") and outDir:
        logging.error("Specify either output-path or output-dir, not both.")
        sys.exit(1)

    if outFile == "stdout" and not doCSV and sys.stdout.isatty():
        logging.error("Will not write binary data on a terminal '%s'" % outFile)
        sys.exit(1)

    if outDir:
        mkdir_p( outDir )
    elif numProcs > 1:
        numProcs = 1
        logging.notice("Cannot use parallel processing for a single output. Reducing process count to 1.")

    flow_params = script.get_flow_params()
    sdate = flow_params.get_start_date()
    edate = flow_params.get_end_date()

    # Retrieve list of all log files in time window
    mapper  = LogMapper( brolog_root )
    mapping = mapper( flow_params )
    #mapping = pool.map(mapper, [parms for parms in flow_params.by_day()])

    # Map results by hour and assign to bins
    _duration = edate - sdate
    _bin_size = _duration // numProcs
    _boundary = sdate + _bin_size
    _bin_size = datetime( _boundary.year, _boundary.month, _boundary.day, _boundary.hour, tzinfo=sdate.tzinfo) - sdate # Floor to nearest hour


    # TODO: This doesn't provide a useful mapping
    #hour_to_tuples = LogPartition( mapping, _bin_size )
    
    filelist = [ x[1] for y in mapping for x in y ]

    pprint.pprint(filelist)
    #items = hour_to_tuples.values()[0]
    #logging.info("Partitions: %s", partition(items, numProcs))
    #logging.info("NUmber of parts: %i", len(partition(items, numProcs)))

    # Generate reduced output
    #reducer = Reducer( "output-dir", _bin_size )
    #flow_sets = pool.map(reducer, partition(items, numProcs))

    #for chunk in hour_to_tuples.values():
    #    reducer(chunk)




script.execute(main)
